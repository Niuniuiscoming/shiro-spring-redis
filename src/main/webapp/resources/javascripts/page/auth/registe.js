var smsContentLength = 6;
var interval_id = null;

$(document).ready(function(){
	initValidate();
	getSmsContent();
	deleteSmsFn();
	validateSmsAndGotoNext();
});

// validate
function initValidate(){
	$.validator.addMethod("mobilePhone", function(value, element) {
		return /^1[35678]\d{9}$/.test(value);
	}, "请输入正确的手机号码");
	$('#registeForm').validate({
        rules: {
        	mobilePhone: {
        		required: true,
        		mobilePhone: true
        	},
        	smsContent: {
            	required: true,
            	rangelength: [6, 6]
            }
        },
        messages : {
        	mobilePhone: {
        		required: "必填"
        	},
        	smsContent: {
        		required: "必填",
            	rangelength: "请输入6位验证码"
            }
        },
        highlight: function(element, errorClass, validClass) {
        	if(element.name == "mobilePhone"){
        		$(element).next('span.input-end-icon').removeClass('success');
           	 	$(element).next('span.input-end-icon').addClass('error');
           	 	$("#getSmsBtn").removeClass("enabled");
           	 	$("#getSmsBtn").addClass("disabled");
        	} else if(element.name == "smsContent"){
        		$("#smsValidateBtn").removeClass("enabled");
        	}
        },
        unhighlight: function(element, errorClass, validClass) {
        	$(".registeErrorMsg").html('');
        	if(element.name == "mobilePhone"){
        		$(element).next('span.input-end-icon').removeClass('error');
   	     	 	$(element).next('span.input-end-icon').addClass('success');
   	     	 	if(!interval_id){
   	     	 		$("#getSmsBtn").removeClass("disabled");
   	     	 		$("#getSmsBtn").addClass("enabled");
   	     	 	}
			 } else if (element.name == "smsContent") {
				 $("#smsValidateBtn").addClass("enabled");
			 }
        },
        errorPlacement: function(error, element) {
        	$(".registeErrorMsg").html(error.text());
        }
    });
	
	// 手机号码key up做验证
	$('#registeForm input').on('keyup blur', function () {
		$("#registeForm input[name='mobilePhone']").valid();
    });
	
}

// 倒计时函数
function timer(intDiff,target) {
	interval_id = setInterval(function() {
		var second = 0;// 时间默认值
		if (intDiff > 0) {
			second = Math.floor(intDiff);
		}else{
			$(target).html("验证码");
			$(target).removeClass("disabled");
			$(target).addClass("enabled");
			clearInterval(interval_id);
			interval_id = null;
			return;
		}
		$(target).html(second + 's后重发');
		intDiff--;
	}, 1000);
} 


// 获取验证码
function getSmsContent(){
	var getSmsUrl = basepath+"/auth/sms/getSms";
	$("#getSmsBtn").click(function(){
		if(!$(this).hasClass("enabled")){
			return;
		}
		$(this).removeClass("enabled");
		$(this).addClass("disabled");  // 设置不可用
		timer(60, $("#getSmsBtn"));
		var mobilePhone = $("input[name='mobilePhone']").val();
		$.ajax({
			url : getSmsUrl,
			type: 'POST',
			dataType : 'jsonp',
			data : {
				"mobilePhone" : mobilePhone
			},
			success : function(res) {
				alert(res.sms);
			},
			error : function() {}
		});
	});
}


// 删除验证码
function deleteSmsFn(){
	$("span[name='deleteSmsBtn']").click(function(){
		$("input[name='smsContent']").val('');
	});
}

// 点击：“下一步”
function validateSmsAndGotoNext(){
	$("#smsValidateBtn").click(function(){
		if(!$(this).hasClass("enabled")){
			return;
		}
		$("#registeForm").submit();
	});
}

